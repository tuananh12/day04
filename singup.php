<?php 
echo "<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <!-- CSS only -->
    
    <title>Document</title>
    
    <style>
        .containerbody {
            width: 600px;
            height: 490px;
            border: solid #006ccb 2px;
            margin-left: 30%;
            margin-top: 30px;
        }

        .containerbody-margin {
            margin-top: 50px;
            margin-left: 95px;
        }

        .label{
            color: red;
        }
        .custom-table {
            border-spacing: 25px;
            border-spacing: 15px;
            border-collapse:initial;
        }

        input[type='date']::-webkit-inner-spin-button,
        input[type='date']::-webkit-calendar-picker-indicator {
        display: none;
        -webkit-appearance: none;
}

        .display {
            width: 82px;
            height: 35px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #fff;
        }

        .display2 {
            width: 230px;
            height: 35px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #000;
        }
        
        .checkbox {
            font-size: 25px;

        }

        .checkbox1 {
            width: 30px;
            height: 18px;
            background-color: #80FF00;
            
            
            
        }

        .datetime{
            width: 155px;
            height: 38px;
            padding-left: 15px;
            border: solid #006ccb 2px;
        }

        .selectbox {
            width: 175px;
            height: 38px;
            padding-left: 15px;
            border: solid #006ccb 2px;

        }
        


        #signup {
            margin-left: 30%;

        }

        .customsignup {
            color: white;
            background-color: rgb(0, 162, 40);
            height: 50px;
            width: 140px;
            border: #006ccb;
            border-radius: 10px;
            font-size: 20px;
            border: solid #006ccb 2px;
        }
    </style>
</head>

<body>

    <div class='containerbody'>
        <div class='containerbody-margin'>
            <div style=' margin-left: 20px; width: 450px;'>
            </div>
            <form action='' method='POST'>
            
            <table class='custom-table' >
";

if ($_SERVER['REQUEST_METHOD'] == "POST"){

    $error = array();   

    if(empty($_POST['username'])) {
        $error['username'] = "Hãy nhập tên";
    }else{
        $username = $_POST['username'];
    }

    if(empty($_POST['gender'])) {
        $error['gender'] = "Hãy chọn giới tính";
    }else{
        $gender = $_POST['gender'];
    }

    if(empty($_POST['Khoa'])) {
        $error['Khoa'] = "Hãy chọn phân khoa";
    }else{
        $khoa = $_POST['Khoa'];
    }

    if(empty($_POST['date'])) {
        $error['date'] = "Hãy nhập ngày sinh";
    }else{
        $date = $_POST['date'];
    }

    

    if(!empty($_POST['date'])){
        if(!checkDatetime($_POST['date'])){
            $error['date'] = "Nhập không đúng định dạng";
        }
}

    if(empty($error)) {
        echo "Đã đăng kí thành công";
    }
}

function checkDatetime($data) {
    $reg = "/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/";
    if(preg_match($reg, $data)) {
        return true;
    }else{
        return false;
    }
}


echo
    "
            
                <tr>
                    <td class='display' style='background-color: #66CC00;'>Họ và tên <label class='label'>*</label></td>
                    <td><input class='display2' type='text' name='username' value = '";echo isset($_POST['username']) ? $_POST['username'] : ''; echo "'></td>
                    ";
                    if (isset($error['username'])){
                    echo
                        "
                    <span style = 'color: red;'>"; echo $error['username'] ;
                    echo
                    "</span>"; }
                    echo
                        "
                    <br/>
                    
                </tr>

                <tr>
                    <td class='display' style='background-color: #66CC00;'>Giới tính <label class='label'>*</label></td>
                    
                    <td>";
                         $gender = array(0 => 'Nam', 1 => 'Nữ');
                        for ($i = 0; $i < count($gender); $i++) {
                        echo
                        "<input class='checkbox1' type='radio' id='".$i."' class='gender' name='gender' value='".$gender[$i]. "'";
                        echo isset($_POST['gender']) && $_POST['gender'] == $gender[$i] ? " checked" : "";
                        echo "/> ".$gender[$i];
                        }
                    echo
                    "</td> 
                    ";
                    if (isset($error['gender'])){
                    echo
                        "
                    <span style = 'color: red;'>"; echo $error['gender'] ;
                    echo
                    "</span>"; }
                    echo
                        "
                    <br/>

                </tr>

                <tr>
                    <td class='display' style='background-color: #66CC00;'>Phân khoa <label class='label'>*</label></td>
                    <td><select class='selectbox' name = 'Khoa' value = ''>";
                            $khoa = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                            foreach ($khoa as $key => $value) {
                            echo "
                            <option ";
                            echo isset($_POST['Khoa']) && $_POST['Khoa'] == $key ? "selected " : "";
                            echo "  value='" . $key . "'>" . $value . "</option>";
                            }
                            echo "
                        </select></td>
                        ";
                        if (isset($error['Khoa'])){
                        echo
                            "
                        <span style = 'color: red;'>"; echo $error['Khoa'] ;
                        echo
                        "</span>"; }
                        echo
                            "
                        <br/>
 
                </tr>

                

                <tr>
                    <td class='display' style='background-color: #66CC00;'>Ngày sinh <label class='label'>*</label></td>
                    <td><input class='datetime' name = 'date' placeholder = 'dd/mm/yyyy'  type='text' value = '";echo isset($_POST['date']) ? $_POST['date'] : ""; echo "'></td>
                    ";
                    if (isset($error['date'])){
                    echo
                        "
                    <span style = 'color: red;'>"; echo $error['date'] ;
                    echo
                    "</span>"; }
                    echo
                        "
                    <br/>

                </tr>

                <tr>
                    <td class='display' style='background-color: #66CC00;'>Địa chỉ</td>
                    <td><input class='display2' type='text' name = 'Address' value = '";
                    echo isset($_POST['Address']) ? $_POST['Address'] : "";
                    echo "''></td>
                </tr>

            </table>

            <div id='signup'>
                
                <input class='customsignup' type='submit' value='Đăng ký'>
            </div>
            </form>
        </div>
</body>

</html>";